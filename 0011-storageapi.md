Milestone 11 - Storage API

Mon, 16 May 2022 12:33:50 +0200

We are pleased to announce the initial release of the lcsync file syncing and
storage tool:

  https://git.sr.ht/~librecast/lcsync/tree/a93328750819abf43507e0e9c73fca9aecf7799b

This will evolve over time into a drop-in replacement for rsync using multicast,
and more of the back-end functions will get pushed into the Librecast API.

To sync files, each file is split into blocks and a merkle tree is built by
hashing the blocks using BLAKE2S. On the sending/server side, this tree is sent
on Librecast Channel (IPv6 multicast group) that is formed from the hash of the
filename.  The receiver/client joins this channel, and receives the tree.  If
the client already has some data to compare, it builds a merkle tree of the
destination file and uses this to quickly compare which blocks differ. It builds
a bitmap with this information, and then joins the Channel(s) for the block(s)
required which are sent by the server.

There is no unicast communication with the server. There are no requests sent,
and the server can sit behind a firewall which is completely closed to inbound
TCP and UDP traffic.  Instead, the server listens on a raw socket for Multicast
Listener Discovery (MLD2) reports. It compares any MLD multicast group JOINs
against the index it built on startup and finds matches for file (tree) and
blocks. In this way, the server only sends data when at least one client is
subscribed.  If further clients want to download the data, the server need take
no further action.  Thus, the load on the server does not change at all,
regardless of whether there is one client or a billion.

We recently performed experiments through Fed4Fire+ to confirm this method
indeed works as expected and that the server load and bandwidth usage is
unaffected by the number of active clients.

As far as we are aware, no other server software uses this method of "MLD
triggering".  It is essentially the same as MLD snooping that switches use to
determine with ports are active on which multicast groups.

lcsync used an experimental form of MLD triggering.  Instead of using
linked-lists for tracking multicast groups, as the Linux kernel does, I wanted
to test something more scalable. There can potentially be 2^112 multicast groups
in IPv6, so beyond a certain point the O(n) search on a linked-list does not
scale. lcsync uses SIMD (CPU vector operations) to implement counted bloom
filters, as well as what I'm calling a "bloom timer", which lets us track active
multicast groups in O(1) constant time.  This is complete overkill for most uses
of lcsync, but is potentially a requirement for a router with a very large
number of active groups.  The MLD implementation in lcsync really belongs in
lcroute and vice-versa, but the lcsync one was developed first.  We plan to
combine the two approaches and make it a compile-time option in Librecast.

The use of merkle trees gives us many options for sharding, as a client can
request any block or part of a file by joining the group for the appropriate
merkle subtree. Thus, a client can choose to sync the whole file, half, a
quarter etc. right down to a single block.  We intend to use this facility to
allow blocks to be distributed among nodes.  Any client could then reconstitute
the whole file by JOINing the appropriate groups.  The data will be fetched from
whichever node(s) happen to have those blocks.

This approach also gives us the ability to de-duplicate data, as any duplicate
files or blocks will have the same hashes.  Instead of syncing the data as
files, we plan to add the option to sync all files to a data store, which will
store the de-duplicated data from which the files can be reconstructed.  This
will allow for snapshot backups etc. to be very efficiently stored and then
checked out, a bit like from a git repository.
