# Guidance on writing Documentation for the project

## Introduction

This document is an ongoing guide for adapting documentation from code sources.

## API Calls

API calls are documented in the man pages for the code.  When adapting the man pages for the wiki, check various typesetting instructions and change them to MD format.

Symlinks within man pages need to be made direct links to the main page for that API Call.
