[[Home]]

# Name

Description

## Install

Dependencies (if applicable)

## Usage

## Questions, Bug reports, Feature Requests

New issues can be raised at:

https://codeberg.org/librecast/project/issues

It's okay to raise an issue to ask a question.  You can also email or ask on
IRC.

## Dev Status

# Licence

This work is dual-licensed under GPL 2.0 and GPL 3.0.

SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
