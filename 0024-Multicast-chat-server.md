Milestone 24 - Multicast Chat Server

Tue, 16 Nov 2021 18:24:58 +0100

As part of our continuing work on assembling the front end to Librecast LIVE
we've finally strapped in the long awaited chat server component and uploaded
the front-end code to:

https://git.sr.ht/~librecast/librecast-live

This is a complete rewrite of the previous version using only vanilla javascript
and no frameworks or front-end UI toolkits.  Just HTML5, CSS3 and modern
javascript.  Node/webpack/babel build dependencies have been stripped out to
keep this as simple as possible.

The code is built up in a react.js style reactive, state-driven, component based
way, but purely in vanilla javascript.  After experimenting with JSX, Typescript
and TSX, and building a compiler for this, I found it was better (faster, more
maintainable) to just re-write in pure javascript instead.

You can sign up and view a demo at:

https://live.librecast.net/

Michiel Leenars has seen a demo of a much earlier version of this before the
rewrite.  The new one is much prettier!

(NB: requires a fairly modern browser, as we haven't transpiled the code yet to
polyfill support for older browsers)  Live development site, so expect things to
move fast and break often.

The back-end is running LSD with the websockets module, the LSD-MULTICAST
daemon, lcauth, and of course, the librecast C library.

Note: Direct-user messaging and E2E encryption are coming in milestones 25 + 26
along with more multicast telemetry to expose the workings.
