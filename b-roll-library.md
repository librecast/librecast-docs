# B-Roll Library

Distributed, searchable b-roll (video) library with:

- audio descriptions (and translations)
- tags
