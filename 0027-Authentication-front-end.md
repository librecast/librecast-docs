Milestone 27 - Authentication (Front End)

Mon, 8 Nov 2021 13:08:28 +0100

This is the milestone (27) for the front-end authentication in the browser.
Milestone 10 is the matching back-end authentication which was claimed for
separately.

Most of this code was written over a year ago while working on milestone 10, but
it was never uploaded or claimed for.  I've just updated and rewritten large
parts of it as part of assembling the front end for Librecast Live.

This code depends on librecast.js which has also been rewritten as part of this
milestone to:
* simplify
* remove dependencies
* decomplicate build system (remove webpack etc in favour of make)
* use modern javascript features such as Promises and arrow functions

  https://git.sr.ht/~librecast/librecast.js

  
  
This library manages the websocket setup and communication with the Librecast
(IPv6 multicast) backend.  The Context/Socket/Channel classes mirror the C
functions that are available on the server, using a kind of RPC callback system
to execute commands over the websocket to manage the multicast communications on
the server.  This gives us a way to connect over unicast and IPv4 into the IPv6
multicast backend directly from the browser.

I helped found and have been participating in the W3C Multicast Community Group
(https://www.w3.org/community/multicast/) to try and enable multicast directly
in the browser.  This is largely being driven by a developer from Akamai, and
their only real interest is in read-only video streaming in the browser.  While
useful, that isn't sufficient for what we're trying to achieve with Librecast,
although we may be able to use it for the incoming streams for Librecast LIVE,
we'll still need tunnelling for our outbound multicast.  Once again, even the
"experts" on multicast don't understand its potential.

The authentication code (the bit this claim mainly pertains to) is in a separate
repository here:

   https://git.sr.ht/~librecast/lcauth

You can see it in action on our live development site:

  https://live.librecast.net/

(NB: that site isn't "live" yet, and I'm actively developing things there, so it
may be subject to some breakage)

Explanation of how it works (same as Milestone 10):

All communication is over IPv6 multicast and end-to-end encrypted.  The web
front end uses a HTTP websocket to connect to the back-end multicast network.
Each node creates its own multicast group address by generating a public keypair
and using a hash of the public key as the 112 bit group address.  This makes it
possible to tell who "owns" a particular group address, as all communications
are encrypted and signed using the matching public key.

A user creates an account by entering their email address.  This is encrypted,
signed and sent to the multicast address for the auth server.  The user client
software has generated a keypair (two actually, one for signing, another for
encryption).

Upon receiving the request and decrypting it, the auth server creates a random
single use token and emails this to the new user after storing it on disk
(LMDB).  The token has a configurable expiry (default: 1 hour).  A response code
(OK/ERROR) is sent back to the address matching the public key of the user.

As each interaction with the auth server is a simple request/response, there is
no need to negotiate a symmetric session key for further communication.  Other
services which are more "chatty" (such as the text and video chat rooms) will
use symmetric keys.

The user clicks the link with the token.  They are prompted for a password, and
their password and token are encrypted, signed and sent to the auth server.
"Forgot password", "Change Password" and "Create Account" are all identical
processes.  Only the email template differs.

The auth server validates the token, deletes it, and sets the password for this
user.  A response code is sent back to the user.  Passwords are stored hashed
and salted using libsodium.  We invent no crypto here and have been careful to
follow best practice for password storage.

To login, the user sends their email address and password back to the auth
server.  If verified, the auth server generates and signs a capability token
which can be used to authenticate with any other part of the system.  These have
a limited expiry time.  There is presently no token revocation system as the
tokens are short-lived, but one could be added to make kick/banning users more
effective.

I've tried to describe a flexible, distributed multicast authentication system
that can be used for more than just this project.  It has a simple, extensible
binary protocol.  The format of the capability tokens means they could be used
for just about anything.

