Milestone 4 - IPv6 Multicast C Library

Fri, 31 Jul 2020 19:27:47 +0100

Code review and productionise library.

I've split the library into three separate parts and removed some redundant
code.  There are now separate headers and shared libraries for:

- IPv6 Multicast messaging
- local database commands and querying
- remote (multicast) database commands

The code now compiles using either gcc or clang.  There is a "make clang" target
in the Makefile.

I've added a test runner and a set of test modules to the project to exercise
all the main functions, including common error conditions.  This will continue
to be added to as I have adopted test driven development for the project.

`make test` runs all the tests.
`make check` runs all the tests using valgrind to do leak checking and dynamic
analysis.

`make 0000-0004.test` runs a single test.
`make 0000-0004.check` runs a single test with valgrind
`make 0000-0004.debug` runs a single test with the gdb debugger
`make sparse` compiles the project using cgcc (the sparse static analyser)
`make clang` builds the project using clang
`make coverity` builds the project using the coverity static analyser, creating
a librecast.tgz ready to upload to Coverity Scan for analysis.

The code is available here:

https://git.sr.ht/~librecast/librecast/tree/6f24db7b1a654d8d3026ae4f7ccc6f714877808a


