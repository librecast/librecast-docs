Milestone 8 - Encryption (messaging)

Wed, 18 May 2022 15:38:58 +0200

Added symmetric encryption support to librecast C library messaging. This is not
a substitute for end-to-end encryption which will be used in many applications,
but can be used where E2EE is not required (or where the endpoint is the C
program calling librecast).

Added key management functions to the core Librecast library and the ability to
set encoding per channel.  This will also be used for enabling Forwards Error
Correction (FEC) etc.  When encoding is enabled on a channel the normal API
functions for sending and receiving automatically deal with encryption and
decryption.

Different keys can be set per channel.

Memory holding the keys is mlock()ed to ensure it isn't swapped to disk.

The code is available in the main branch and will be included in the
upcoming v0.4.6 release.

https://git.sr.ht/~librecast/librecast/tree/91ed44906de07d871b2c7efe4f9c64f5732177e0


