Milestone 5: Multicast Module

Wed, 30 Sep 2020 11:10:47 +0100

Wrote a modular multicast daemon that can be configured to run different
multicast modules and listen on different groups.

The code is available here:

https://git.sr.ht/~librecast/lsd-multicast
