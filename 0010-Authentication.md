Milestone 10 - Authentication

Wed, 30 Sep 2020 12:09:49 +0100

This is the milestone (10) for the back-end authentication module, which runs
under lsd-multicast.  Milestone 27 is the matching front-end authentication
which will be claimed for separately.

We're using libsodium for all of the crypto, as it is small, well-tested and has
a C library and also a javascript implementation which we use for the front end.

While it was tempting to use client certificates or authentication using the
public keys themselves, these are ephemeral and it is assumed that a user may
have multiple devices.  A password is more portable than a certificate, and our
security needs are relatively low.  The system could be extended to require
multi-factor authentication if later required. Once authenticated with the auth
server, all further authentication with other parties uses signed capability
tokens.

All communication is over IPv6 multicast and end-to-end encrypted.  The web
front end uses a HTTP websocket to connect to the back-end multicast network.
Each node creates its own multicast group address by generating a public keypair
and using a hash of the public key as the 112 bit group address.  This makes it
possible to tell who "owns" a particular group address, as all communications
are encrypted and signed using the matching public key.

A user creates an account by entering their email address.  This is encrypted,
signed and sent to the multicast address for the auth server.  The user client
software has generated a keypair (two actually, one for signing, another for
encryption).

Upon receiving the request and decrypting it, the auth server creates a random
single use token and emails this to the new user after storing it on disk
(LMDB).  The token has a configurable expiry (default: 1 hour).  A response code
(OK/ERROR) is sent back to the address matching the public key of the user.

As each interaction with the auth server is a simple request/response, there is
no need to negotiate a symmetric session key for further communication.  Other
services which are more "chatty" (such as the text and video chat rooms) will
use symmetric keys.

The user clicks the link with the token.  They are prompted for a password, and
their password and token are encrypted, signed and sent to the auth server.
"Forgot password", "Change Password" and "Create Account" are all identical
processes.  Only the email template differs.

The auth server validates the token, deletes it, and sets the password for this
user.  A response code is sent back to the user.  Passwords are stored hashed
and salted using libsodium.  We invent no crypto here and have been careful to
follow best practice for password storage.

To login, the user sends their email address and password back to the auth
server.  If verified, the auth server generates and signs a capability token
which can be used to authenticate with any other part of the system.  These have
a limited expiry time.  There is presently no token revocation system as the
tokens are short-lived, but one could be added to make kick/banning users more
effective.

I've tried to describe a flexible, distributed multicast authentication system
that can be used for more than just this project.  It has a simple, extensible
binary protocol.  The format of the capability tokens means they could be used
for just about anything.


The code is available here:

https://git.sr.ht/~librecast/lsd-multicast
