[[Home]]

# Name

## Synopsis

his template covers features and tools created under NGI Zero Discovery.


## Install

### Distribution

If in Repology include graphic.

Create install instructions per nix or per distribution.


## Usage

## Questions, Bug reports, Feature Requests

New issues can be raised at:

https://codeberg.org/librecast/project/issues

It's okay to raise an issue to ask a question.  You can also email or ask on
IRC.

## Dev Status

# Licence

This work is dual-licensed under GPL 2.0 and GPL 3.0.

SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

# Funding

<p class="bigbreak">
This project was funded through the <a href="https://nlnet.nl/discovery"> NGI0 Discovery </a> Fund, a fund established by NLnet with financial support from the European
Commission's <a href="https://ngi.eu">Next Generation Internet</a> programme, under the aegis of DG Communications Networks, Content and Technology under grant agreement No 825322. *Applications are still open, you can <a href="https://nlnet.nl/propose">apply today</a>*
</p>

  <a href="https://nlnet.nl/project/LibrecastLive/">
      <img width="250" src="https://nlnet.nl/logo/banner.png" alt="Logo NLnet: abstract logo of four people seen from above" class="logocenter" />
  </a>
  <a href="https://ngi.eu/">
      <img width="250" align="right" src="https://nlnet.nl/image/logos/NGI0_tag.png" alt="Logo NGI Zero: letterlogo shaped like a tag" class="logocenter" />
  </a>
</p>
