Milestone 16 - Librecast Router Daemon

Sun, 20 Feb 2022 21:01:04 +0100

We now have our own from-scratch IPv6 Multicast Router Daemon (lcroute). This is
one of the fundamental building blocks for the Librecast Project, including
federation of Librecast LIVE servers.  We've been giving lcroute a good
thrashing on the Fed4Fire+ testbeds over the past couple of months as part of
our file transfer tests.

With lcroute and lctunnel combined, we can connect IPv6 multicast networks and
route traffic, even through NAT and over IPv4 networks.

Code is here:

https://git.sr.ht/~librecast/lcroute

