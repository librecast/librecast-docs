[[Home]] > [[API|api]]

The API call documentation is collated from various man pages as detailed in the DOC folder in source.


# API CALL NAME <DATE> "LIBRECAST" "Librecast Programmer's Manual"

## API Call - Description

## SYNOPSIS

``Compile instructions as code blob``

## DESCRIPTION 

Detail calls with call name.  Explain what the call does and why it's there


## RETURN VALUE 

Detail what the API call should return.

## ERRORS

Detail possible Errors.

SEE ALSO:

Link to other pages.


 