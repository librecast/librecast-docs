# Name

## Synopsis

## Requirements

## Objectives

### architectures
### protocols
### standard
### Measurability

#### Stakeholders



### Logic specification


## Functional specification

### Project scope -- the goals, features, tasks, deliverables, costs and deadlines of the project.

###  Risks and assumptions  -- the considerations that could affect the functional design of the product.

###  Product overview -- the explanation of how the application will solve a specific problem for the target audience.

### Use cases -- the functional requirements are placed in the context of a user action. This shows what happens from the user perspective.

### Requirements -- essential features of the product that explain what it does.

### Configuration -- steps needed to configure a product, such as user account setup.

### Non-functional requirements -- the non-essential features that aren’t at the core of the product.

### Error reporting -- an explanation of how the product will handle errors or exceptions.



## Business requirements

 This document describes the business and stakeholder It also describes the high-level goals an organization is trying to achieve or the needs it’s trying to fulfill by developing a service or product.

## System requirements specification 

This details what requirements must be fulfilled to satisfy the needs of the business. As a structured document, the SRS describes the functional requirements, non-functional requirements and any use cases that the software must fulfill. 

## Functional requirements document (FRD).

 The FRD describes exactly how the system should function to meet all the requirements noted in the BRD and SRS. The FRD expands on all the details pertaining to the functional requirements on a project.

